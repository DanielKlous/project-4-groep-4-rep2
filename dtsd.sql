-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Gegenereerd op: 26 jun 2018 om 11:52
-- Serverversie: 5.7.22-0ubuntu0.16.04.1
-- PHP-versie: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dtsd`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `login_attempts`
--

CREATE TABLE `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`) VALUES
(1, 'Denis', 'Denis@denis.com', '00807432eae173f652f2064bdca1b61b290b52d40e429a7d295d76a71084aa96c0233b82f1feac45529e0726559645acaed6f3ae58a286b9f075916ebf66cacc', 'f9aab579fc1b41ed0c44fe4ecdbfcdb4cb99b9023abb241a6db833288f4eea3c02f76e0d35204a8695077dcf81932aa59006423976224be0390395bae152d4ef');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `weetjes`
--

CREATE TABLE `weetjes` (
  `weetjeID` int(11) NOT NULL,
  `date` date NOT NULL,
  `weetje` text NOT NULL,
  `approved` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `weetjes`
--

INSERT INTO `weetjes` (`weetjeID`, `date`, `weetje`, `approved`) VALUES
(1, '1991-06-16', 'Op Zondag, 16. Juni 1991 werd RKC Waalwijk - FC Twente maarliefst 6-6! Doelpunten kwamen onder andere van Marc van Hintum en Jan van Halst.', 1),
(2, '2005-02-02', 'In 2005 op 2 februari kreeg PSV tegen RBC Roosendaal 4 keer een strafschop. De eerste twee werden gemist, maar daarna schoten Mark van Bommel en Jan Vennegoor of Hesselink de strafschoppen er wel in. Het eindigde uiteindelijk in 4-1.', 1),
(3, '2000-06-02', 'In het seizoen 2003/2004 hielden Heurelho Gomes en Edwin Zoetebier 1159 minuten het doel schoon. Angelos Charisteas maakte een einde aan de reeks.', 1),
(4, '2018-06-05', 'Sander Boschker is een clubicoon en speelde de meeste wedstrijden ooit in het shirt van FC Twente. Dit deed hij maarliefst 556 keer!', 1),
(5, '2018-06-06', 'De jongste scorende debutant is Jairo Riedewald. In het seizoen 2013/2014 verving hij tegen Roda JC in de 80e minuut Christian Poulsen. kurwa', 1),
(6, '2018-07-06', 'Voetbal is begonnen met een blaas van een koe als bal.', 1),
(14, '2018-06-05', 'dfsdfdsfsdfs', 0),
(15, '2018-06-05', 'blablabla1234', 0),
(16, '2018-06-05', '', 0),
(17, '2018-06-05', 'hoi', 0);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `weetjes`
--
ALTER TABLE `weetjes`
  ADD PRIMARY KEY (`weetjeID`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `weetjes`
--
ALTER TABLE `weetjes`
  MODIFY `weetjeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
