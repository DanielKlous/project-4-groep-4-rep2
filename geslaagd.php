<?php 
	session_start(); 

	if (!isset($_SESSION['username'])) {
		$_SESSION['msg'] = "eerst moet je inloggen";
		header('Location: login.php');
	}

	if (isset($_GET['logout'])) {
		session_destroy();
		unset($_SESSION['username']);
		header("Location: login.php");
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<style>
	body {
		background: linear-gradient(to right, #304352 0%,/* #b21f1f 50%, */#d7d2cc 100%);
	}
	</style>
</head>
<body>
	<div class="header">
		<h2>Home Page</h2>
	</div>
	<div class="content">


		<?php // if (isset($_SESSION['success'])) : ?>
			<div class="error success" >
                <h3>
					<?php 
                    echo $_SESSION['success'];
                    unset($_SESSION['success']);
					?>
				</h3>
			</div>
		<?php// endif ?>


		<?php  if (isset($_SESSION['username'])) : ?>
			<p>Welkom <strong><?php echo $_SESSION['username']; ?></strong></p>
			<p> <a href="geslaagd.php?logout='1'" style="color: red;">Uitloggen</a> </p>
		<?php endif ?>
	</div>
		
</body>
</html>