<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kalender Pagina Wistjedatjes</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#datepicker").datepicker({dateFormat: 'mm-dd'})
        });
    </script>
    <style>
        body {
            background: linear-gradient(to right, #304352 0%, /* #b21f1f 50%, */ #d7d2cc 100%);
            margin: 0px;
            padding: 0px;
        }

        #nav {
        }

        #nav-toggle {
            position: absolute;
            cursor: pointer;
            background: transparent;
            padding: 27px 50px 33px 12px;
            display: none;
        }

        #nav-toggle span, #nav-toggle span:before, #nav-toggle span:after {
            position: absolute;
            content: '';
            display: block;
            background: white;
            width: 40px;
            height: 7px;
            border-radius: 1px;
            transition: all 500ms ease;
        }

        #nav-toggle span:before {
            top: -10px;
        }

        #nav-toggle span:after {
            bottom: -10px;
        }

        #nav-toggle.active span:before, #nav-toggle.active span:after {
            top: 0;
        }

        #nav-toggle.active span {
            background: transparent;
        }

        #nav-toggle.active span:before {
            transform: rotate(45deg);
        }

        #nav-toggle.active span:after {
            transform: rotate(-45deg);
        }

        ul#navbar {
            list-style-type: none;
            width: 44%;
            padding-left: 55px;

            margin-top: 6px;
        }

        ul#navbar li {
            display: inline-block;
            font-size: 1.5em;
        }

        ul#navbar li a {
            text-decoration: none;
            display: block;
            padding: 10px;
            color: #FF0000;
            font-family: "Gill Sans", "Gill Sans MT", Calibri, sans-serif;
            background: rgba(255, 170, 212, 1);
            transition: all 0.3s ease;
        }

        ul#navbar li a:hover {
            color: #ffffff;
            background: #FF0000;
        }

        @media (max-width: 480px) {
            #nav-toggle.active {
                position: absolute;
                animation: rise 0.3s ease-in-out;
                top: 11%;
            }

            #nav-toggle {
                top: 40%;
                left: 42%;
                animation: drop 1s ease-in-out;
                display: block;
            }

            #nav {
                display: none;
            }

            ul#navbar {
                margin-top: 125px;
                padding-left: 0;
            }

            ul#navbar li {
                display: block;
            }

            ul#navbar li a {
                text-align: center;
            }

        }

        @media (min-width: 481px) and (max-width: 767px) {
            #nav-toggle.active {
                position: absolute;
                animation: rise 0.3s ease-in-out;
                top: 11%;
            }

            #nav-toggle {
                animation: drop 1s ease-in-out;
                top: 40%;
                left: 45%;
                display: block;
            }

            #nav {
                display: none;
            }

            ul#navbar {
                margin-top: 125px;
                padding-left: 0;
            }

            ul#navbar li {
                display: block;
            }

            ul#navbar li a {
                text-align: center;
            }

        }

        @media (min-width: 768px) and (max-width: 992px) {
            #nav-toggle {
                left: 0;
                display: block;
                border-radius: 0;
            }

            #nav {
                display: none;
            }

            ul#navbar {
                margin-left: 16px;
                animation: slide 0.3s ease;
            }

            ul#navbar li {
                margin-top: 10px;
            }
        }

        @media (min-width: 992px) {
            #nav-toggle {
                left: 0;
                display: block;
                border-radius: 0;
            }

            #nav {
                display: none;
            }

            ul#navbar {
                margin-left: 16px;
                animation: slide 0.3s ease;
            }

            ul#navbar li {
                margin-top: 10px;
            }
        }

        @keyframes drop {
            from {
                top: 10%;
            }
            to {
                top: 40%;
            }
        }

        @keyframes rise {
            from {
                top: 40%;
            }
            to {
                top: 11%;
            }
        }

        @keyframes slide {
            from {
                margin-left: -500px;
            }
            to {
                margin-left: 16px;
            }
        }

        .h1 {
            position: absolute;
            left: 0px;
            right: 0px;
            top: 0px;
            bottom: 0px;
            margin: auto auto;
            text-align: center;
            float: center;
            color: black;
            padding: 10px;
            text-align: center;
            font-family: "Jua", sans-serif;
            width: 20%;
            height: auto;
        }

        p {
            color: black;
        }

        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            width: 200px;
        }

        li a {
            display: block;
            color: #000;
            padding: 8px 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: #555;
            color: white;
        }

        img {
            border-radius: 8px;
        }

        .content{
            margin-top: 80px;
        }

    </style>

</head>
<img src="Mediabureau_Stuif_en_deVries_BV.png" style="width: 15%; height: 15%; z-index: 10;"/>
<h1 class="h1">Kalender</h1>
<div id="menu-icon">
    <a href="#" id="nav-toggle" style="border-radius: 1rem;"><span></span></a>
</div>
<div id="nav">
    <ul id="navbar">
        <li><a href="index.php" style="background-color: transparent;">Home</a></li>
        <li><a href="login.php" style="background-color: transparent;">Login pagina</a>
        </li><li><a href="weetjes_toevoegen.php" style="background-color: transparent;">Weetje toevoegen</a>
        </li><li><a href="kalender.php" style="background-color: transparent;">kalender</a></li>
    </ul>
</div>
<script>
    $(document).ready(function() {
        $('#nav-toggle').click(function(event) {
            $(this).toggleClass('active');
            if ($('#nav-toggle').hasClass('active'))
            {
                $('#nav').show();
            }
            else
            {
                $('#nav').hide();
            };
        });
    });
</script>
<body>
<div class="content">
    <form action="" method="post">
        <p>Kalender: <input type="text" id="datepicker" autocomplete="off" name="dateinput"></p>
        <input type="submit" name="submitkalender" value="tonen">
    </form>
</div>
</body>
</html>
<?php
//Connect to the database
//$mysqli = NEW MySQLi('localhost','root','','test');
$mysqli = new MySQLi('128.199.45.193', 'myuser', 'mypass', 'dtsd');

if ($mysqli->connect_error) {
    die("Connection failed: " . $mysqli->connect_error);
}

if (isset($_POST['submitkalender'])) {

//Query the database by current date independent of year.
    $sDate = date("m-d"); //$_GET['date'];
    $date = $_POST['dateinput'];
    function dutchDate($date)
    {
        return date("m-d", strtotime($date));
    }

    $resultSet = $mysqli->query("SELECT * FROM weetjes WHERE approved = 1 AND `date` LIKE '%$date%'");
    if ($resultSet->num_rows > 0) {
        //Turn the results into an array

        while ($rows = $resultSet->fetch_assoc()) {
            $weetjeID = $rows['weetjeID'];
            $date = dutchDate($rows['date']);
            $weetje = $rows['weetje'];

            echo "<br><br><strong style='font-family: Tahoma, Geneva, sans-serif; font-size: 26px;'>Weetje van de dag</strong><br><p style='font-size: 24px;'>$date<br />$weetje</p>";

        }
        //Display the results
    } else {
        echo "ERROR: Geen weetjes gevonden.";
    }
    function isSecondFebruary()
    {
        $month = date('n');
        $day = date('j');
        return ($month == 2 && $day >= 2); // || ($month == 1 && $day <= 6);
    }

    echo isSecondFebruary();
}
?>
